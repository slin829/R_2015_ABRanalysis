audiogram <- function(type, rm.id=NULL){
  source("aud_data.R")
  var <- dir("./audiogram_data")
  v <- sub(".csv", "", var)
  ave <- vector()
  for(treatment in v){
    data_int <- aud_data(type, treatment, rm.id)
    ave <- rbind(ave, data_int)
  }
  
  limits.b= aes(ymax= base + base.sd, ymin= base-base.sd)
  limits.f= aes(ymax= final + final.sd, ymin= final-final.sd)
  limits.s= aes(ymax= shift + shift.sd, ymin= shift-shift.sd)
  if(type=="baseline"){
    plot <- ggplot(ave, aes(colour=Treatment, x=Frequency, y=base)) 
  }else if(type=="final"){
    plot <- ggplot(ave, aes(colour=Treatment, x=Frequency, y=final))  
  }else if (type=="shift"){
    plot <- ggplot(ave, aes(colour=Treatment, x=Frequency, y=shift))  
  }
  
  plot <- plot + geom_point(size=3) +
    geom_line(size =1 ,aes(group=Treatment)) + 
    labs(title =  paste("ABR ", type, sep="")) +
    scale_y_continuous(limits=c(-30,100),breaks=c(seq(0, 90, by=20)),
                       name="Threshold (dB)") + 
    geom_errorbar(limits.b, size =1 ,width = 0.2)      

  plot
}
