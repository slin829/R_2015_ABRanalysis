lat <- function(freq, export= TRUE){
  #freq= "4k"
  var = dir("./abr_io")
  var= as.vector(var)
  var<- var[var!="no_sound"]
  var<- var[var!="saline"]
  var<- var[var!="saline_plug"]
  var<- var[var!="saline_sun"]
  var<- var[var!="notes.txt"]
  var<- var[var!="A1101"]
  var<- var[var!="A1102"]
  var<- var[var!="A1103"]
  var<- var[var!="A1104"]
  var<- var[var!="A1113"]
  var<- var[var!="A1204"]
  
  data=vector()
  for (v in var){
    if(!(paste(freq,".csv",sep="")) %in% dir(paste("./abr_io/",v, sep=""))){
      next
    }
    int_data <- read.csv(paste("./abr_io/",v,"/",freq,".csv",sep=""), header=TRUE)
    int_data <- cbind(int_data$Sub..ID, int_data$Level.dB., int_data$T1.ms., int_data$V1.uv., int_data$T2.ms., int_data$V2.uv.)
    colnames(int_data) <- c("Sub_ID", "Sound_level", "Peak_lat", "Peak_amp", "Trough_lat", "Trough_amp")
    int_data <- as.data.frame(int_data) 
    data <- rbind(data, int_data)
  }
  #amp_diff <- data[,"Peak_amp"]-data[,"Trough_amp"]
  #data <- cbind(data, amp_diff)
  #data <-na.omit(data)
  
  Treatment=vector(length=nrow(data))
  data= cbind(data, Treatment)
  for(i in 1:nrow(data)){
    if(data[i,1]>1100 & data[i,1]<1200){
      data[i, "Treatment"] = "control_s"
    }
    if(data[i,1]>1200 & data[i,1] <1300){
      data[i, "Treatment"] = "control_k"
    }
  }
  
  subdata =subset(data, data$Treatment %in% "control_s")
  
  library(ggplot2)
  my_plot<- ggplot(data, aes(Sound_level, Peak_lat, colour=Treatment))+
                geom_point(size=3) +
                geom_smooth(size=1) +
                labs(title= freq) +
                scale_y_continuous(limits=c(0,8),name="Latency (ms)") +
                scale_x_continuous(limits=c(0,90), name="Sound level (dB)") + 
                theme(axis.title=element_text(size=20, face="bold"),
                      axis.text=element_text(size=20,face="bold"),
                      legend.text=element_text(size=15, face="bold"),
                      legend.title=element_text(size=15),
                      legend.position = "right",
                      plot.title= element_text(size=20, face="bold")) +
                guides(colour = guide_legend(override.aes = list(size=10), nrow=2))
  my_plot
  

}
