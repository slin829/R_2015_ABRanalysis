fn <- function(x){
  F2k= 2000/x
  F60= 60/x
  F50 = 50/x
  tab=rbind(F2k,F60, F50)
  tab=as.data.frame(tab)
  colnames(tab) <- paste(x, "Hz", sep="")
  tab
}

